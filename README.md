# 介绍

Node Express Web 框架

## 结构

```
|-- express-server
    |-- config          // 项目配置模块
    |-- controller      // 业务控制模块
    |-- db              // 数据库模块
    |-- files           // 文件存放目录 
    |-- router          // 路由模块
    |-- rules           // 数据规则校验模块
    |-- sql             // sql表结构目录
    |-- utils           // 工具包模块
    |-- .gitignore
    |-- app.js          // 项目入口
    |-- package.json    // 项目依赖管理
    |-- README.md
```

## 中间件

- body-parser 数据解析(json、x-www-form-urlencoded)
- multer 数据解析(form-data 文件上传)
- cors 跨域资源共享
- jwt express-jwt 身份认证
- joi express-joi 数据校验

# 运行

```sh
# 依赖下载
npm i
# 项目运行
npm run dev
```