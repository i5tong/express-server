const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { expressjwt } = require('express-jwt');
const joi = require('joi');
const config = require('./config/config');

// 路由
const sysRouter = require('./router/system');
const userRouter = require('./router/user');
const fileRouter = require('./router/file');

// 创建 Web 服务器
const app = express();

// 注册 bodyParser 中间件
app.use(bodyParser.json()); // 解析 json 数据格式
app.use(bodyParser.urlencoded({ extended: true })); // 解析 form 表单数据 application/x-www-form-urlencoded
// 注册 Cors 跨域资源共享
app.use(cors());
// 托管静态资源
app.use('/files', express.static('./files'));
// 注册数据响应中间件
app.use((req, res, next) => {
  res.cc = (err, codeStatus, data) => {
    const { code = 1, status = 500 } = codeStatus || {};
    if (code === 1) {
      console.error(`Error Msg >>>>>> ${err}`);
    } else {
      console.log(err);
    }
    res.send({
      code, status, msg: err instanceof Error ? err.message : err, data,
    });
  };
  next();
});
// 注册 express-jwt 中间件
const { secret, path, algorithms } = config.jwt
app.use(expressjwt({ secret, algorithms }).unless({ path })); // unless 排除
// 注册路由
app.use('/sys', sysRouter);
app.use('/api/v1', userRouter);
app.use(fileRouter);
// 注册错误处理中间件（路由之后）
app.use((err, req, res, next) => {
  console.error(`Error: ${err.message}`);
  if (err instanceof joi.ValidationError) {
    res.cc('数据验证失败');
  } else if (err.name === 'UnauthorizedError') {
    res.cc('无效 Token', { status: 401 });
  } else {
    res.cc('内部服务错误');
  }
  next();
});

// 项目启动
app.listen(7001, () => {
  console.log('Express 项目启动：http://localhost:7001');
});