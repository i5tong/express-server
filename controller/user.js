const db = require('../db/mysql');
const util = require('../utils/util');

// 用户基本信息
exports.userInfo = (req, res) => {
  // 解析 Token
  const { userId } = req.auth;
  const sql = 'select username, email, status from users where id = ?';
  db.query(sql, userId, (err, queryRes) => {
    if (err) {
      res.cc(err);
      return;
    }
    if (queryRes.length < 1) {
      res.cc('用户信息获取失败');
      return;
    }
    const user = queryRes && queryRes[0] || {};
    const data = { ...user, password: '' }
    res.cc('用户基本信息', { code: 0, status: 200 }, user);
  })
}

// 用户列表
exports.list = (req, res) => {
  const sql = 'select id, username, email, status from users';
  db.query(sql, (err, queryRes) => {
    if (err) {
      res.cc(err);
      return;
    }
    res.cc('用户列表', { code: 0, status: 200 }, queryRes);
  })
}

// 更新用户信息
exports.update = (req, res) => {
  const id = req.params.id;
  const data = req.body;
  const sql = 'update users set ? where id = ?';
  db.query(sql, [ data, id ], (err, updateRes) => {
    if (err) {
      res.cc(err);
      return;
    }
    if (updateRes.affectedRows !== 1) {
      res.cc('更新用户信息失败');
      return;
    }
    res.cc('更新用户信息成功', { code: 0, status: 200 });
  })
}

// 重置密码
exports.reset = (req, res) => {
  // 解析 Token
  const { userId } = req.auth;
  const data = req.body;
  let sql = 'select * from users where id = ?';
  db.query(sql, userId, (err, queryRes) => {
    if (err) {
      res.cc(err);
      return;
    }
    if (queryRes.length !== 1) {
      res.cc('当前用户不存在');
      return;
    }
    const user = queryRes && queryRes[0];
    // 密码验证
    const flag = util.compare(data.oldPwd, user.password);
    if (!flag) {
      res.cc('原密码错误');
      return;
    }
    // 修改原密码
    sql = 'update users set password = ? where id = ?';
    const newPwd = util.bcrypt(data.newPwd);
    db.query(sql, [ newPwd, userId ], (err, updateRes) => {
      if (err) {
        res.cc(err);
        return;
      }
      if (updateRes.affectedRows !== 1) {
        res.cc('重置密码失败');
        return;
      }
      res.cc('重置密码成功', { code: 0, status: 200 });
    })
  })
}

// 更新用户头像
exports.avatar = (req, res) => {
  // 解析 Token
  const { userId } = req.auth;
  const data = req.body;
  let sql = 'select * from users where id = ?';
  db.query(sql, userId, (err, queryRes) => {
    if (err) {
      res.cc(err);
      return;
    }
    if (queryRes.length !== 1) {
      res.cc('当前用户不存在');
      return;
    }
    // 修改用户头像
    sql = 'update users set avatar = ? where id = ?';
    db.query(sql, [ data.avatar, userId ], (err, updateRes) => {
      if (err) {
        res.cc(err);
        return;
      }
      if (updateRes.affectedRows !== 1) {
        res.cc('更新用户头像失败');
        return;
      }
      res.cc('更新用户头像成功', { code: 0, status: 200 });
    })
  })
}