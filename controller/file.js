// 单文件上传
exports.single = (req, res) => {
  const data = req.body;
  const file = req.file;
  res.cc('单文件上传成功', { code: 0, status: 200 }, { data, file });
}

// 多文件上传
exports.multi = (req, res) => {
  const data = req.body;
  const files = req.files;
  res.cc('多文件上传成功', { code: 0, status: 200 }, { data, files });
}