const db = require('../db/mysql');
const util = require('../utils/util');

// 用户注册
exports.register = (req, res) => {
  const data = req.body;
  let { username, password } = data;
  let sql = 'select * from users where username = ?'; // 查询
  db.query(sql, username, (err, queryRes) => {
    if (err) {
      res.cc(err);
      return;
    }
    if (queryRes.length >= 1) {
      res.cc('用户已存在');
      return;
    }
    password = util.bcrypt(password, 10)
    sql = 'insert into users set ?'; // 插入
    db.query(sql, { username, password }, (err, insertRes) => {
      if (err) {
        res.cc(err);
        return;
      }
      if (insertRes.affectedRows !== 1) {
        res.cc('用户注册失败');
        return;
      }
      res.cc('用户注册成功', { code: 0 });
    });
  });
}

// 用户登录
exports.login = (req, res) => {
  const data = req.body;
  const { username, password } = data;
  let sql = 'select * from users where username = ?'; // 查询
  db.query(sql, username, (err, queryRes) => {
    if (err) {
      res.cc(err);
      return;
    }
    if (queryRes.length < 1) {
      res.cc('用户不存在');
      return;
    }
    const user = queryRes && queryRes[0] || {};
    const encrypted = user.password;
    const flag = util.compare(password, encrypted);
    if (flag) {
      // jwtToken
      const token = util.jwtToken({ userId: user.id });
      res.cc('用户登录成功', { code: 0, status: 200 }, { token: `Bearer ${token}` });
    } else {
      res.cc('用户登录失败');
    }
  });
}