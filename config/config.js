const jwt = {
  secret: 'lovot',
  path: [ /^\/sys\//, /^\/files\// ], // String | RegExp | {}
  algorithms: [ 'HS256' ]
}

module.exports = {
  jwt
}