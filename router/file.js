const express = require('express');
const path = require('path')
const multer = require('multer')
const fileController = require('../controller/file');
const expressJoi = require('@escook/express-joi');
const fileRules = require('../rules/file');

// 创建路由对象
const router = express.Router();

// 定义文件存放路径
// const upload = multer({ dest: path.join(__dirname, '../files') });
const storage = multer.diskStorage({
  // 指定文件存放路径
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname, '../files'));
  }, // 设置文件名称
  filename: (req, file, cb) => {
    cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
  }
});
const upload = multer({ storage });

/*
 * 局部中间件 multer 解析 form-data 数据，文件类型数据挂载至 req.file 文本类型数据挂载至 req.body
 * 中间件 先 multer 解析数据，后 joi 验证数据
 * upload.single 指定文件 fieldname 信息
 * upload.array 指定文件 fieldname 信息以及文件数
 */
router.post('/files/single', upload.single('lovot'), expressJoi(fileRules.uploadRule), fileController.single);
router.post('/files/multi', upload.array('lovot', 3), expressJoi(fileRules.uploadRule), fileController.multi);

// 导出路由对象
module.exports = router;