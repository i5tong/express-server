const express = require('express');
const userController = require('../controller/user');
const expressJoi = require('@escook/express-joi');
const userRules = require('../rules/user');

// 创建路由对象
const router = express.Router();

router.get('/users/userInfo', userController.userInfo); // 用户基本信息
router.get('/users', userController.list); // 用户列表
router.put('/users/:id', expressJoi(userRules.updateRule), userController.update); // 更新用户信息
router.post('/users/reset', expressJoi(userRules.resetRule), userController.reset); // 重置用户密码
router.post('/users/avatar', expressJoi(userRules.avatarRule), userController.avatar); // 更新用户头像

// 导出路由对象
module.exports = router;