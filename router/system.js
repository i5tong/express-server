const express = require('express');
const expressJoi = require("@escook/express-joi");
const userRules = require("../rules/user");
const sysController = require("../controller/system");

// 创建路由对象
const router = express.Router();

// 健康检查
router.get('/health', (req, res) => {
  res.send('健康检查');
})
// 注册
router.post('/register', expressJoi(userRules.registerRule), sysController.register);
// 登录
router.post('/login', expressJoi(userRules.loginRule), sysController.login);

// 导出路由对象
module.exports = router;