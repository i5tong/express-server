const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

/**
 * 加密
 * @param {String} text 加密数据
 * @param {String|Number} salt 加盐长度 默认：10
 * @return {String} 加密文本
 */
exports.bcrypt = (text, salt = 10) => {
  return bcrypt.hashSync(text, salt); // 多次加密结果各不相同
}

/**
 * 校验
 * @param {String} text 原文数据（参数）
 * @param {String} encrypted 加密数据（数据库）
 * @return {Boolean} 对比结果
 */
exports.compare = (text, encrypted) => {
  return bcrypt.compareSync(text, encrypted);
}

/**
 * JWT 签名
 * @param {string|Buffer|object} data 签名数据
 * @param {string|Buffer} secret 密钥
 * @param {string|number|undefined} expiresIn 有效期
 * @return {string} 签名 Token
 */
exports.jwtToken = (data, secret = 'lovot', expiresIn = '1d') => {
  return jwt.sign(data, secret, { expiresIn });
}
