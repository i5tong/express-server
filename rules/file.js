const joi = require('joi')

const title = joi.string().required()
const content = joi.string().required().allow('')
const state = joi.string().valid('已发布', '草稿').required()

exports.uploadRule = {
  body: { title, content, state }
}