const joi = require('joi')

/**
 * string() 值必须是字符串
 * alphanum() 值只能是包含 a-zA-Z0-9 的字符串
 * min(length) 最小长度
 * max(length) 最大长度
 * required() 值是必填项，不能为 undefined
 * pattern(正则表达式) 值必须符合正则表达式的规则
 * joi.ref('oldPwd') 表示 oldPwd 与 newPwd 值保持一致
 * joi.not(joi.ref('oldPwd')) 表示 oldPwd 与 newPwd 值不能相同
 * concat 合并验证规则
 * dataUri 验证 base64 格式图片
 */

const id = joi.number().integer().min(1).required();
const username = joi.string().alphanum().min(1).max(10).required();
const password = joi.string().required();
const newPwd = joi.not(joi.ref('oldPwd')).concat(password); // 新密码不能等同于原密码且验证规则与原密码一致
const email = joi.string().email();
const avatar = joi.string().dataUri().required(); // data:image/png;base64,VE9PTUFOWVNFQ1JFVFM=

exports.registerRule = {
  // 检验 body
  body: { username, password, email }
}

exports.loginRule = {
  // 检验 body
  body: { username, password }
}

exports.updateRule = {
  params: { id }, // 检验 params
  body: { email } // 检验 body
}

// 重置密码校验 规则相同且新密码不能等同于原密码
exports.resetRule = {
  body: { oldPwd: password, newPwd } // 检验 body
}

exports.avatarRule = {
  body: { avatar } // 校验 body
}