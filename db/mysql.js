const mysql = require('mysql');

// 创建数据库连接
const db = mysql.createPool({
  host: 'lovot.top', // 数据库域名或IP地址
  user: 'root', // 数据库账户
  password: '123456', // 数据库密码
  database: 'express', // 指定数据库
})

db.query('select 1 = 1', () => {
  console.log('Mysql 数据库连接成功！')
})

module.exports = db;